/*
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org>
 */

import core.math;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.format;
import std.json;
import std.range;
import std.stdio;
import std.string;
import std.typecons;

import ae.sys.file : cached;
import ae.utils.array;
import ae.utils.json;
import ae.utils.xmllite;
import ae.sys.net.cachedcurl;

struct SegmentMap
{
	string type, viewableId;

	struct Segment
	{
		struct UI
		{
			int[2][] interactionZones;
		}
		UI ui;
		int startTimeMs;
		int endTimeMs;
		struct Next { int weight; }
		Next[string] next;
		string defaultNext;
		bool storyEnd, credits;
	}
	Segment[string] segments;
	string initialSegment;
	struct UI { bool resetBookmarkInCredits; }
	UI ui;
}
SegmentMap segmentMap;
static this() { segmentMap = readText("../en/assets/SegmentMap.js").findSplit("=")[2].jsonParse!SegmentMap; }

struct ImpressionData
{
	string type;
	struct Data
	{
		JSONFragment[string] persistent;
	} Data data;
}
struct Image
{
	string[string] styles;
}

struct Bandersnatch
{
	struct Video
	{
		struct Atom(T)
		{
			@JSONName("$type") string type;
			@JSONName("$size") long size;
			T value;
		}
		struct InteractiveVideoMoments
		{
			string type;
			struct ChoicePointNavigatorMetadata
			{
				struct Config
				{
					string lockStrategy;
					string selectionType;
					bool playerControlsTenSecondsControls;
					int maxSnapshotsToDisplay;
					string timelineContainerOverrideClass;
					bool timelineShowColumnLabel;
					struct Breakpoint
					{
						int cols;
						int minWidth;
					} Breakpoint[] timelineLayoutBreakpoints;
				} Config config;
				struct Storylines
				{
					struct Storyline
					{
						string id;
						struct Chapter
						{
							string id;
							string[] source;
							string[] target;
						} Chapter[] chapters;
					} Storyline[] list;
				} Storylines storylines;
				string type;
				struct ChoicePointsMetadata
				{
					string[] timelineLabel;
					struct ChoicePoint
					{
						string[] choices;
						long startTimeMs;
						string description;
						Image image;
					} ChoicePoint[string] choicePoints;
					string choices;
				} ChoicePointsMetadata choicePointsMetadata;
			} ChoicePointNavigatorMetadata choicePointNavigatorMetadata;
			struct CommonMetadata
			{
				// struct Layout
				// {
				// 	struct Timer
				// 	{
				// 		int minPercentage;
				// 		bool simpleBarTimer;
				// 		bool preventInAndOutAnimation;
				// 		bool timerAlwaysVisible;
				// 		string type;
				// 		string layoutType;
				// 	} Timer timer;
				// } Layout[string] layouts;
				JSONFragment layouts;
				struct Settings
				{
					bool randomInitialDefault;
					bool disableToggleDefault;
				} Settings settings;
			} CommonMetadata commonMetadata;
			string[] segmentHistory;
			JSONFragment[string] stateHistory;
			string[] snapshots;
			struct Moment
			{
				string type;
				long startMs, endMs;
				JSONFragment precondition;
				ImpressionData impressionData;
				long[2] activationWindow;
				string id;
				string segmentId;
				string layoutType;
				long uiDisplayMS, uiHideMS;
				int defaultChoiceIndex;
				long choiceActivationThresholdMS;
				struct Choice
				{
					string id;
					string sg;
					string segmentId;
					string optionType; // !!!
					long startTimeMs;
					ImpressionData impressionData;
					JSONFragment trackingInfo;
					string text;
					Image image;
					string code;
				}
				Choice[] choices;
				string[string] trackingInfo;
				long uiInteractionStartMS;
				struct Config
				{
					bool intervalBasedVideoTimer;
					bool disableImmediateSceneTransition;
					bool disablePrematureUserInteraction;
					bool hideChoiceLabelWhenChoiceHasImage;
					bool randomInitialDefault;
					string defaultSelectType;
					bool hasMultipleChoiceInput;
				} Config config;
				JSONFragment inputConfig;
				string[string] action;
			}
			Moment[][string] momentsBySegment;
			JSONFragment[string] preconditions;
			string audioLocale;
			JSONFragment[][string] segmentGroups;
		}
		Atom!InteractiveVideoMoments interactiveVideoMoments;
	}
	Video[string] videos;
}
Bandersnatch bandersnatch;
static this() { bandersnatch = readText("../en/assets/bandersnatch.js").findSplit("=")[2].jsonParse!Bandersnatch; }

Bandersnatch.Video.InteractiveVideoMoments* video;
static this() { video = &bandersnatch.videos["80988062"].interactiveVideoMoments.value; }

enum optimize = true;

enum bestVariableReadOrderFN = "varorder-read.txt";
enum bestVariableWriteOrderFN = "varorder-write.txt";
bool[string][string] allVariableValues;
string[] varReadOrder, varWriteOrder;
bool draft;
static this()
{
	void scanImpressionData(ref ImpressionData impressionData)
	{
		foreach (name, value; impressionData.data.persistent)
			allVariableValues[name][value.json.strip] = true;
	}

	foreach (id, moments; video.momentsBySegment)
		foreach (moment; moments)
		{
			scanImpressionData(moment.impressionData);
			foreach (choice; moment.choices)
				scanImpressionData(choice.impressionData);
		}

	varReadOrder  = optimize && bestVariableReadOrderFN .exists ? bestVariableReadOrderFN .readText.splitLines : allVariableValues.keys.sort.release;
	varWriteOrder = optimize && bestVariableWriteOrderFN.exists ? bestVariableWriteOrderFN.readText.splitLines : allVariableValues.keys.sort.release;
}

enum numVars = 62;

struct ParsedData
{
	struct Precondition
	{
		string id;

		struct Expr
		{
			enum Kind
			{
				none,
				literal,
				var,
				not,
				and,
				or,
				eql,
			}
			Kind kind;

			union
			{
				VarValue value;   // Kind.literal
				size_t varIndex; // Kind.var
			}
			Expr[] children; // Kind.and / or / eql

			this(VarValue value) { kind = Kind.literal; this.value = value; }
			this(size_t varIndex) { kind = Kind.var; this.varIndex = varIndex; }
			this(Kind kind, Expr[] children) { this.kind = kind; this.children = children; }
		}
		Expr expr;

		T opCast(T)() const if (is(T==bool)) { return expr.kind != Expr.Kind.none; }
	}
	Precondition[] preconditions;

	struct VarAssignment
	{
		size_t varIndex;
		ubyte value;
	}
	alias Impression = VarAssignment[];

	struct Segment
	{
		string id;
		struct Moment
		{
			Precondition precondition;
			Impression impression;
			struct Choice
			{
				size_t targetSegmentIndex;
				size_t targetSegmentGroupIndex;

				Impression impression;
				string text;
			}
			Choice[] choices;
		}
		Moment[] moments;
		size_t preconditionIndex;
		size_t segmentGroupIndex;
		size_t defaultNextSegmentIndex;
	}
	Segment[] segments;

	struct SegmentGroup
	{
		string id;

		struct Part
		{
			enum Kind
			{
				segment,
				segmentWithPrecondition,
				segmentGroup,
			}
			Kind kind;

			size_t segmentIndex;
			alias segmentGroupIndex = segmentIndex;
			size_t preconditionIndex;
		}
		Part[] parts;
	}
	SegmentGroup[] segmentGroups;

	alias VarValue = ubyte;
	struct Var
	{
		string name;
	}
	Var[] vars;

	size_t initialSegmentIndex;
}
ParsedData data;
static this()
{
	// Create lookups

	data.segments = segmentMap.segments
		.byPair
		.array
		.sort!((a, b) => a[1].startTimeMs < b[1].startTimeMs)
		.map!(pair => ParsedData.Segment(pair[0]))
		.array;
	auto segmentLookup = data.segments
		.enumerate
		.map!(pair => tuple(pair[1].id, pair[0]))
		.assocArray;

	data.segmentGroups = video.segmentGroups
		.byKey
		.map!(id => ParsedData.SegmentGroup(id))
		.array;
	auto segmentGroupLookup = data.segmentGroups
		.enumerate
		.map!(pair => tuple(pair[1].id, pair[0]))
		.assocArray;

	data.preconditions = video.preconditions
		.byKey
		.map!(id => ParsedData.Precondition(id))
		.array;
	auto preconditionLookup = data.preconditions
		.enumerate
		.map!(pair => tuple(pair[1].id, pair[0]))
		.assocArray;

	data.vars = allVariableValues.keys.sort.map!(name => ParsedData.Var(name)).array;
	assert(data.vars.length == numVars);
	auto varLookup = data.vars
		.enumerate
		.map!(pair => tuple(pair[1].name, pair[0]))
		.assocArray;

	ParsedData.VarValue convertValue(JSONValue v)
	{
		if (v.type == JSONType.false_)
			return 0;
		else
		if (v.type == JSONType.true_)
			return 1;
		else
		if (v.type == JSONType.string && v.str.length == 1)
			return v.str[0];
		else
			assert(false);
	}

	ParsedData.Precondition convertPrecondition(JSONFragment precondition, string id = null)
	{
		alias Expr = ParsedData.Precondition.Expr;
		Expr parseExpr(JSONValue v)
		{
			if (v.type.among(JSONType.false_, JSONType.true_, JSONType.string))
				return Expr(convertValue(v));
			else
			if (v.type == JSONType.array)
				switch (v.array[0].str)
				{
					case "persistentState":
						assert(v.array.length == 2);
						return Expr(varLookup[v[1].str]);
					case "not":
						assert(v.array.length == 2);
						return Expr(Expr.Kind.not, [parseExpr(v[1])]);
					case "and":
						return Expr(Expr.Kind.and, v.array[1..$].map!parseExpr.array);
					case "or":
						return Expr(Expr.Kind.or , v.array[1..$].map!parseExpr.array);
					case "eql":
						assert(v.array.length == 3);
						return Expr(Expr.Kind.eql, v.array[1..$].map!parseExpr.array);
					default:
						assert(false);
				}
			else
				assert(false, v.toString());
		}
		if (precondition.json)
			return ParsedData.Precondition(id, parseExpr(parseJSON(precondition.json)));
		else
			return ParsedData.Precondition.init;
	}

	ParsedData.Impression convertImpression(ref ImpressionData impressionData)
	{
		if (impressionData is ImpressionData.init)
			return null;
		assert(impressionData.type == "userState", impressionData.type);
		return impressionData.data.persistent
			.byPair
			.map!(pair => ParsedData.VarAssignment(varLookup[pair[0]], convertValue(pair[1].json.parseJSON)))
			.array;
	}

	foreach (ref segment; data.segments)
	{
		segment.moments =
			video.momentsBySegment.get(segment.id, null)
			.map!(moment => ParsedData.Segment.Moment(
				convertPrecondition(moment.precondition),
				convertImpression(moment.impressionData),
				moment.choices.map!(choice => ParsedData.Segment.Moment.Choice(
					choice.segmentId ? segmentLookup     [choice.segmentId] : size_t.max,
					choice.sg        ? segmentGroupLookup[choice.sg       ] : size_t.max,
					convertImpression(choice.impressionData),
					choice.text,
				)).array,
			))
			.array;

		segment.preconditionIndex = preconditionLookup.get(segment.id, size_t.max);
		segment.segmentGroupIndex = segmentGroupLookup.get(segment.id, size_t.max);

		auto defaultNext = segmentMap.segments[segment.id].defaultNext;
		segment.defaultNextSegmentIndex = segmentMap.segments[segment.id].defaultNext ? segmentLookup[defaultNext] : size_t.max;
	}

	foreach (ref segmentGroup; data.segmentGroups)
		segmentGroup.parts = video.segmentGroups[segmentGroup.id].map!(
			(part)
			{
				auto json = parseJSON(part.json);
				alias Part = ParsedData.SegmentGroup.Part;
				if (json.type == JSONType.string)
					return Part(Part.Kind.segment,
						segmentLookup[json.str]);
				else
				if (json.type == JSONType.object)
				{
					if (json.object.keys.sort.equal(["precondition", "segment"]))
						return Part(Part.Kind.segmentWithPrecondition,
							segmentLookup[json["segment"].str],
							preconditionLookup[json["precondition"].str]);
					else
					if (json.object.byKey.equal(["segmentGroup"]))
						return Part(Part.Kind.segmentGroup,
							segmentGroupLookup[json["segmentGroup"].str]);
					else
						assert(false, part.json);
				}
				else
					assert(false, part.json);
			}
		).array;

	foreach (ref precondition; data.preconditions)
		precondition = convertPrecondition(video.preconditions[precondition.id], precondition.id);

	data.initialSegmentIndex = segmentLookup[segmentMap.initialSegment];
}

struct TextData
{
	string[string] segmentDescriptions;
	struct Var
	{
		string readText;
		string[string] readValueText, writeValueText;
	}
	Var[string] vars;
	string[][string] momentDescriptions;
}
TextData textData;
static this()
{
	enum dataFn = "data.org";
	enum sanitizedDataFn = "data.json";

	if (dataFn.exists && (!sanitizedDataFn.exists || sanitizedDataFn.timeLastModified < dataFn.timeLastModified))
	{
		TextData textData;
		auto lines = dataFn.readText().splitLines();
		string[] segmentLines, varLines, momentLines, macroLines;
		string section;
		foreach (line; lines)
			if (line.startsWith("*"))
				section = line;
			else
			if (line.startsWith("| "))
			{
				if (section == "* Macros")
					macroLines ~= line;
				else
				if (section == "* Segments")
					segmentLines ~= line;
				else
				if (section == "* Variables")
					varLines ~= line;
				else
				if (section == "* Moments")
					momentLines ~= line;
			}

		string[2][] macros;
		foreach (line; macroLines[1..$])
		{
			auto parts = line.split("|");
			if (parts[1].length)
				macros ~= [parts[1].strip, parts[2].strip];
		}
		macros.sort!((a, b) => a[0].length > b[0].length);

		string replaceMacros(string s)
		{
			foreach (pair; macros)
				s = s.replace(pair[0], pair[1]);
			return s;
		}

		foreach (line; segmentLines[1..$])
		{
			auto parts = line.split("|");
			auto desc = parts[5].strip;
			if (desc.length)
				textData.segmentDescriptions[parts[1].strip] = replaceMacros(desc);
		}

		foreach (line; varLines[1..$])
		{
			auto parts = line.split("|").map!strip.array;
			TextData.Var var;
			var.readText = parts[3].length ? replaceMacros(parts[3]) : null;
			foreach (col; parts[4 .. $])
				if (col.length)
				{
					auto colParts = col.split("#").map!strip.array;
					if (colParts.length > 1)
						var.writeValueText[colParts[0]] = replaceMacros(colParts[1]);
					if (colParts.length > 2)
						var.readValueText[colParts[0]] = replaceMacros(colParts[2]);
				}
			textData.vars[parts[1]] = var;
		}

		foreach (line; momentLines[1..$])
		{
			auto parts = line.split("|");
			auto desc = parts[6].strip;
			if (desc.length)
				textData.momentDescriptions.require(parts[1].strip).putExpand(parts[2].strip.to!size_t, replaceMacros(desc));
		}

		textData.toPrettyJson.toFile(sanitizedDataFn);
	}

	textData = sanitizedDataFn.readText.jsonParse!TextData;
}

void main(string[] args)
{
	writeln(segmentMap.segments.length, " segments");
	writeln(allVariableValues.length, " variables");
	writeln(video.momentsBySegment.byValue.joiner.filter!(m => m.choices).walkLength, " choices");
	writeln(video.segmentGroups.length, " segment groups");
	writeln(video.preconditions.length, " preconditions");

	if (args.length == 1)
	{
		//checkContiguity();
		//genSegmentGraph();
		genGraph();
		genDataTemplates();
		genHTML();
	}
	else
		switch (args[1])
		{
			case "opt":
				return optimizeVars();
			case "explore":
				return explore();
			default:
				assert(0);
		}
}

void genSegmentGraph()
{
	auto f = File("segments.dot", "w");
	f.writeln("digraph {");
	foreach (name, segment; segmentMap.segments)
	{
		foreach (nextName, next; segment.next)
			f.writefln("\t%(%s%) -> %(%s%);", [name], [nextName]);
	}
	f.writeln("}");
}

void explore()
{
	alias VarState = ParsedData.VarValue[numVars];

	static struct State
	{
		size_t segmentIndex;
		VarState vars;

		void applyImpression(ref ParsedData.Impression impression)
		{
			foreach (ref assignment; impression)
				vars[assignment.varIndex] = assignment.value;
		}

		bool checkPrecondition(ref ParsedData.Precondition precondition) const
		{
			alias Expr = ParsedData.Precondition.Expr;
			ParsedData.VarValue eval(Expr expr)
			{
				final switch (expr.kind)
				{
					case Expr.Kind.none:
						assert(false);
					case Expr.Kind.literal:
						return expr.value;
					case Expr.Kind.var:
						return vars[expr.varIndex];
					case Expr.Kind.not:
						return !eval(expr.children[0]);
					case Expr.Kind.and:
						return expr.children.map!eval.all;
					case Expr.Kind.or:
						return expr.children.map!eval.any;
					case Expr.Kind.eql:
						return eval(expr.children[0]) == eval(expr.children[1]);
				}
			}
			auto result = eval(precondition.expr);
			assert(result <= 1);
			return !!result;
		}

		bool applySegmentGroup(size_t segmentGroupIndex)
		{
			foreach (part; data.segmentGroups[segmentGroupIndex].parts)
				final switch (part.kind)
				{
					case ParsedData.SegmentGroup.Part.Kind.segment:
					{
						auto segment = &data.segments[part.segmentIndex];
						if (segment.preconditionIndex == size_t.max || checkPrecondition(data.preconditions[segment.preconditionIndex]))
						{
							segmentIndex = part.segmentIndex;
							return true;
						}
						break;
					}
					case ParsedData.SegmentGroup.Part.Kind.segmentWithPrecondition:
						if (checkPrecondition(data.preconditions[part.preconditionIndex]))
						{
							segmentIndex = part.segmentIndex;
							return true;
						}
						break;
					case ParsedData.SegmentGroup.Part.Kind.segmentGroup:
						if (applySegmentGroup(part.segmentGroupIndex))
							return true;
						break;
				}
			return false;
		}
	}

	enum breadthFirst = false;
	enum rememberPath = false;

	static struct Visitor
	{
		enum Step
		{
			start,
			defaultNext,
			segmentGroup,
			choice1,
			choice2,
		}
		struct Breadcrumb
		{
			static if (rememberPath)
			{
				State parentState;
				Step step;
			}
			else
				this(in ref State state, Step step) {}
		}

		Breadcrumb[VarState][] visited;

		static if (breadthFirst)
			State[] queue;

		State initialState;

		void run()
		{
			visited.length = data.segments.length;

			initialState.segmentIndex = data.initialSegmentIndex;
			visitNext(initialState, initialState, Step.start);

			static if (breadthFirst)
				while (queue.length)
				{
					auto node = &queue.shift();
					scope(failure) printPath(*node);
					visit(*node);
				}
		}

		void printPath(State state)
		{
			string[] lines;
			static string segmentText(size_t segmentIndex) { auto id = data.segments[segmentIndex].id; return id ~ " - " ~ textData.segmentDescriptions.get(id, "?"); }
			lines ~= segmentText(state.segmentIndex);
			while (state != initialState)
			{
				auto breadcrumb = visited[state.segmentIndex][state.vars];
				static if (rememberPath)
				{
					state = breadcrumb.parentState;
					if (breadcrumb.step >= Step.choice1)
						lines ~= "\t" ~ data.segments[state.segmentIndex].moments[$-1].choices[breadcrumb.step - Step.choice1].text;
					else
						lines ~= "\t" ~ text(breadcrumb.step);
					lines ~= segmentText(state.segmentIndex);
				}
				else
				{
					lines ~= "\t?";
					break;
				}
			}
			foreach_reverse (line; lines)
				stderr.writeln(line);
		}

		void visitNext(in ref State state, in ref State parentState, Step step)
		{
			if (state.vars in visited[state.segmentIndex])
				return;
			visited[state.segmentIndex][state.vars] = Breadcrumb(parentState, step);

			static if (breadthFirst)
				queue ~= state;
			else
			{
				scope(failure) stderr.writefln("%s <- %s <- %s",
					data.segments[state.segmentIndex].id,
					step,
					data.segments[parentState.segmentIndex].id,
				);
				visit(state);
			}
		}

		void visit(in ref State parentState)
		{
			State state = parentState;

			static int counter;
			if (counter++ % 10000 == 0)
				stderr.writefln("Visit %-4s %-(%s%)", data.segments[state.segmentIndex].id, state.vars[].map!(c => cast(char)(c < 2 ? '0' + c : c)));

			auto segment = &data.segments[state.segmentIndex];
			bool canContinue = true;
			foreach (ref moment; segment.moments)
			{
				assert(canContinue);
				if (moment.precondition !is ParsedData.Precondition.init && !state.checkPrecondition(moment.precondition))
					continue;
				state.applyImpression(moment.impression);
				if (moment.choices)
				{
					canContinue = false;
					foreach (choiceIndex, ref choice; moment.choices)
					{
						State next = state;
						auto step = cast(Step)(Step.choice1 + choiceIndex);
						next.applyImpression(choice.impression);
						if (choice.targetSegmentIndex != size_t.max)
						{
							next.segmentIndex = choice.targetSegmentIndex;
							visitNext(next, parentState, step);
						}
						else
						if (choice.targetSegmentGroupIndex != size_t.max)
						{
							if (next.applySegmentGroup(choice.targetSegmentGroupIndex))
								visitNext(next, parentState, step);
							else
								assert(false); // canContinue = true; // TODO: what does the official player do?
						}
						else
							canContinue = true;
					}
				}
			}
			if (!canContinue)
				return;

			if (segment.segmentGroupIndex != size_t.max)
			{
				if (state.applySegmentGroup(segment.segmentGroupIndex))
					return visitNext(state, parentState, Step.segmentGroup);
				else
				{
					assert(false); // TODO: what does the official player do?
				}
			}
			if (segment.defaultNextSegmentIndex != size_t.max)
			{
				state.segmentIndex = segment.defaultNextSegmentIndex;
				return visitNext(state, parentState, Step.segmentGroup);
			}

			//stderr.writeln(" >> Dead end?");
		}
	}

	Visitor visitor;
	visitor.run();

	//stderr.writefln("Explored %d states.", visited.length);
	foreach (i; 0 .. data.segments.length)
	{
		writefln("%-4s - %d states", data.segments[i].id, visitor.visited[i].length);
		if ("states".exists)
		{
			auto f = File("states/" ~ data.segments[i].id, "w");
			foreach (varState; visitor.visited[i].keys.sort)
				f.rawWrite(varState);
		}
	}
}

struct Graph
{
	alias NodeIndexVal = size_t;
	struct NodeIndex
	{
		NodeIndexVal val = NodeIndexVal.max;
		bool opCast(T)() if (is(T==bool)) { return val != NodeIndexVal.max; }
	}
	struct Edge
	{
		enum Kind { undefined, normal, segment, choiceDefault, segmentGroupDefault, nextDefault, yes, no, unreachable }
		Kind kind;
		NodeIndex target;
		string label;
		bool reversed;
		bool noCap;

		void put(NodeIndex source, ref File f)
		{
			if (reversed)
				f.writef("\tn%d -> n%d [dir=back]", target.val, source.val);
			else
				f.writef("\tn%d -> n%d"           , source.val, target.val);
			if (label)
				f.writef(" [label=%(%s%)]", [label]);
			if (noCap)
				f.write(" [arrowhead=none]");
			final switch (kind)
			{
				case Kind.undefined          : assert(false);
				case Kind.normal             : break;
				case Kind.segment            : f.write(" [penwidth=2] [color=\"#bbeeee\"]"); break;
				case Kind.choiceDefault      : f.write(" [penwidth=2]"); break;
				case Kind.segmentGroupDefault: f.write(" [penwidth=2] [style=dashed]"); break;
				case Kind.nextDefault        : f.write(" [penwidth=2] [style=dotted]"); break;
				case Kind.yes                : f.write(" [penwidth=2] [color=\"#00bb00\"]"); break;
				case Kind.no                 : f.write(" [penwidth=2] [color=\"#ff0000\"] [style=dashed]"); break;
				case Kind.unreachable        : f.write(" [style=dotted]"); break;
			}
			f.writeln(";");
		}
	}
	struct Node
	{
		enum Kind { segment, ending, recap, precondition, preconditionResult, impression, choices, choice, segmentGroup, dummy, point, error }
		Kind kind;
		string label;
		enum Format { text, html, image }
		Format format;
		string tooltip;
		Edge[] edges;
		bool deleted, gc, main;

		void put(NodeIndex index, ref File f)
		{
			f.writef("\tn%d", index.val);
			if (label)
				final switch (format)
				{
					case Node.Format.text:
						f.writef(" [label=%(%s%)]", [label]);
						break;
					case Node.Format.html:
						f.writef(" [label=<%s>]", label.replace(`</b> <b>`, ` `));
						break;
					case Node.Format.image:
						f.writef(` [label="" image=%(%s%)]`, [label]);
						break;
				}

			if (tooltip)
				f.writef(" [tooltip=%(%s%)]", [tooltip]);
			final switch (kind)
			{
				case Kind.segment           : f.write(" [shape=box    , fillcolor=\"#bbeeee\", color=\"#446666\",                  style=filled, penwidth=2]"); break;
				case Kind.ending            : f.write(" [shape=box    , fillcolor=\"#bbeebb\", color=\"#446644\",                  style=filled, penwidth=2]"); break;
				case Kind.recap             : f.write(" [shape=box    , fillcolor=\"#dddddd\", color=\"#555555\",                  style=filled, penwidth=2]"); break;
				case Kind.precondition      : f.write(" [shape=diamond, fillcolor=\"#ffffee\", color=\"#ffffaa\", fontcolor=black, style=filled, penwidth=2]"); break;
				case Kind.preconditionResult: f.write(" [shape=oval   , fillcolor=\"#ffffee\", color=\"#ffffaa\", fontcolor=black, style=filled, penwidth=2]"); break;
				case Kind.impression        : f.write(" [shape=box    , fillcolor=\"#ffeeff\", color=\"#ffaaff\", fontcolor=black, style=filled, penwidth=2]"); break;
				case Kind.choices           : f.write(" [shape=diamond, fillcolor=\"#bbeeee\", color=\"#446666\", fontcolor=black, style=filled, penwidth=2]"); break;
				case Kind.choice            : f.write(" [shape=box    , fillcolor=\"#000000\", color=\"#ffffff\", fontcolor=white, style=filled, penwidth=2]"); break;
				case Kind.segmentGroup      : assert(false);
				case Kind.dummy             : assert(false);
				case Kind.point             : f.write(" [shape=point  , width=0,               color=\"#ffffff\",                  style=invis             ]"); break;
				case Kind.error             : f.write(" [shape=box    , fillcolor=\"#eebbbb\", color=\"#664444\",                  style=filled, penwidth=2]"); break;
			}
			f.writeln(";");
		}
	}
	Node[] nodes;

	NodeIndex addNode(Node.Kind kind, string label, Node.Format format = Node.Format.text, string tooltip = null)
	{
		nodes ~= Node(kind, label, format, tooltip);
		return NodeIndex(nodes.length - 1);
	}
	void addEdge(Edge.Kind kind, NodeIndex from, NodeIndex to, string label = null, bool reversed = false)
	{
		nodes[from.val].edges ~= Edge(kind, to, label, reversed);
	}

	NodeIndex firstNode;
	private NodeIndex[] unreachableNodes;

	void put(File f)
	{
		f.writeln(`digraph {`);
		f.writeln(`	graph [bgcolor=black];`);
		f.writeln(`	node [style=filled];`);
		f.writeln(`	edge [color=white fontcolor=white fontname="helvetica"];`);
		f.writeln(`	node [fontname="helvetica"];`);
		f.writeln(``);

		{
			auto startNode = addNode(Node.Kind.choice, "START");
			addEdge(Edge.Kind.normal, startNode, firstNode);
			// nodes[startNode.val].put(startNode, f);
			// nodes[startNode.val].deleted = true;

			auto unreachableNode = addNode(Node.Kind.error, "UNREACHABLE");
			foreach (n; unreachableNodes)
				if (n != firstNode)
					addEdge(Edge.Kind.unreachable, unreachableNode, n);
			// nodes[unreachableNode.val].put(unreachableNode, f);
			// nodes[unreachableNode.val].deleted = true;

			f.writefln(`	{ rank=same; n%d; n%d; }`, startNode.val, unreachableNode.val);
		}

		foreach (i, ref node; nodes)
			if (!node.deleted && !node.main)
				node.put(NodeIndex(i), f);

		f.writeln(`subgraph cluster_main {`);
		f.writeln(`		color="#222222"`);

		foreach (i, ref node; nodes)
			if (!node.deleted && node.main)
				node.put(NodeIndex(i), f);

		f.writeln(`}`);

		foreach (i, ref node; nodes)
			if (!node.deleted)
				foreach (ref edge; node.edges)
					edge.put(NodeIndex(i), f);

		f.writeln(`}`);
	}

	void finalize()
	{
		bool changes = true;

		void replace(NodeIndexVal from, NodeIndexVal to, bool doDelete = true)
		{
			if (doDelete)
				nodes[from].deleted = true;
			foreach (ref node; nodes)
				if (!node.deleted)
					foreach (ref edge; node.edges)
						if (edge.target.val == from)
							edge.target.val = to;
			changes = true;
		}

		static Edge.Kind mergeEdgeKind(Edge.Kind a, Edge.Kind b)
		{
			if (a == b)
				return a;
			if (a > b)
				swap(a, b);
			if (a.among(Edge.Kind.normal, Edge.Kind.segment, Edge.Kind.nextDefault, Edge.Kind.segmentGroupDefault) &&
				b.among(Edge.Kind.normal, Edge.Kind.segment, Edge.Kind.nextDefault, Edge.Kind.segmentGroupDefault))
				return a;
			return Edge.Kind.undefined;
		}

		string mergeTooltips(R)(R nodes)
		{
			static immutable tooltipDelim = "\n-------\n";
			return nodes
				.map!(node => node
					.tooltip
					.strip
					.split(tooltipDelim)
				)
				.joiner
				.filter!(s => s.length)
				.array
				.sort
				.uniq
				.join(tooltipDelim);
		}

		foreach (nodeIndex, ref node; nodes)
			if (node.kind == Node.Kind.dummy)
			{
				assert(node.edges.length == 1, "Dummy node with not one edge: " ~ node.label);
				assert(node.edges[0].kind == Edge.Kind.normal);
				replace(nodeIndex, node.edges[0].target.val);
			}

	changesLoop:
		static if (optimize)
		while (changes)
		{
			changes = false;

			// Try to shift choices behind identical segment start nodes.
			// Before: o<8=8
			// After : o-o<8
		choiceShiftLoop:
			foreach (nodeIndex, ref node; nodes)
				if (node.kind == Node.Kind.precondition && node.edges.length >= 2)
				{
					bool boolean = nodes[node.edges[0].target.val].kind != Node.Kind.preconditionResult;
					if (boolean)
					{
						auto firstTarget = &nodes[node.edges[0].target.val];
						foreach (ref edge; node.edges)
						{
							auto target = &nodes[edge.target.val];
							if (!target.kind.among(Node.Kind.segment, Node.Kind.ending, Node.Kind.recap, Node.Kind.impression, Node.Kind.choices, Node.Kind.choice) ||
								target.kind != firstTarget.kind ||
								target.label != firstTarget.label ||
								target.edges.length != 1 ||
								mergeEdgeKind(target.edges[0].kind, firstTarget.edges[0].kind) == Edge.Kind.undefined ||
								target.edges[0].label    != firstTarget.edges[0].label ||
								target.edges[0].reversed != firstTarget.edges[0].reversed ||
								target.edges[0].noCap    != firstTarget.edges[0].noCap ||
								(target.kind == Node.Kind.choices && nodes[target.edges[0].target.val].edges.length != 1) ||
								false)
								continue choiceShiftLoop;
						}

						auto newTargetNode = addNode(firstTarget.kind,
							firstTarget.label, firstTarget.format,
							mergeTooltips(node.edges.map!(edge => nodes[edge.target.val])),
						);

						auto edgeKind = node.edges.map!(edge => nodes[edge.target.val].edges[0].kind).reduce!mergeEdgeKind;

						foreach (ref edge; node.edges)
						{
							auto target = &nodes[edge.target.val];
							target.gc = true;
							edge.target = target.edges[0].target;
						}

						auto edge = firstTarget.edges[0];
						edge.kind = edgeKind;
						edge.target = NodeIndex(nodeIndex);

						replace(nodeIndex, newTargetNode.val, false);

						nodes[newTargetNode.val].edges ~= edge;
						continue changesLoop; // nodes was expanded, we are iterating over an old slice
					}
					else
					{
						auto firstChoice = &nodes[node       .edges[0].target.val];
						auto firstTarget = &nodes[firstChoice.edges[0].target.val];
						foreach (ref edge; node.edges)
						{
							assert(edge.kind == Edge.Kind.normal);
							auto choice  = &nodes[edge.target.val];
							assert(choice.kind == Node.Kind.preconditionResult && choice.edges.length == 1 && choice.edges[0].kind == Edge.Kind.normal);
							auto target = &nodes[choice.edges[0].target.val];

							if (!target.kind.among(Node.Kind.segment, Node.Kind.ending, Node.Kind.recap, Node.Kind.impression, Node.Kind.choices, Node.Kind.choice) ||
								target.kind != firstTarget.kind ||
								target.label != firstTarget.label ||
								target.edges.length != 1 ||
								mergeEdgeKind(target.edges[0].kind, firstTarget.edges[0].kind) == Edge.Kind.undefined ||
								target.edges[0].label    != firstTarget.edges[0].label ||
								target.edges[0].reversed != firstTarget.edges[0].reversed ||
								target.edges[0].noCap    != firstTarget.edges[0].noCap ||
								(target.kind == Node.Kind.choices && nodes[target.edges[0].target.val].edges.length != 1) ||
								false)
								continue choiceShiftLoop;
						}

						auto newTargetNode = addNode(firstTarget.kind,
							firstTarget.label, firstTarget.format,
							mergeTooltips(node.edges.map!(edge => nodes[nodes[edge.target.val].edges[0].target.val])),
						);

						auto edgeKind = node.edges.map!(edge => nodes[edge.target.val].edges[0].kind).reduce!mergeEdgeKind;

						foreach (ref edge; node.edges)
						{
							auto edgeToTarget = &nodes[edge.target.val].edges[0];
							auto target = &nodes[edgeToTarget.target.val];
							target.gc = true;
							edgeToTarget.target = target.edges[0].target;
						}

						auto edge = firstTarget.edges[0];
						edge.kind = edgeKind;
						edge.target = NodeIndex(nodeIndex);

						replace(nodeIndex, newTargetNode.val, false);

						nodes[newTargetNode.val].edges ~= edge;
						continue changesLoop; // nodes was expanded, we are iterating over an old slice
					}
				}

			// Merge duplicate nodes

			NodeIndexVal[Node] dups;
			static Node hashable(Node node) { node.tooltip = null; return node; }
			foreach (nodeIndex, ref node; nodes)
				if (!node.deleted)
				{
					auto oldIndex = dups.require(hashable(node), nodeIndex);
					if (oldIndex != nodeIndex)
					{
						nodes[oldIndex].tooltip = mergeTooltips([nodes[oldIndex], node]);
						replace(nodeIndex, oldIndex);
					}
				}
		}

		// No node creation/deletion past this point.

		// Remove caps to point nodes

		foreach (ref node; nodes)
			foreach (ref edge; node.edges)
				if (nodes[edge.target.val].kind == Node.Kind.point)
					edge.noCap = true;

		// Unreachable nodes and garbage collection

		auto nodeReachable = new bool[nodes.length];
		changes = true;
		while (changes)
		{
			changes = false;
			nodeReachable[] = false;

			foreach (ref node; nodes)
				if (!node.deleted)
					foreach (ref edge; node.edges)
						nodeReachable[edge.target.val] = true;

			foreach (nodeIndex, reachable; nodeReachable)
				if (!reachable && !nodes[nodeIndex].deleted && nodes[nodeIndex].gc)
				{
					nodes[nodeIndex].deleted = true;
					changes = true;
				}
		}

		foreach (nodeIndex, reachable; nodeReachable)
			if (!reachable && !nodes[nodeIndex].deleted)
				unreachableNodes ~= NodeIndex(nodeIndex);

		foreach (ref node; nodes)
			if (!node.deleted)
				foreach (ref edge; node.edges)
					if (nodes[edge.target.val].label == "Rewind!") // TODO: localization
						edge.reversed = true;

		// Main vs. rewind/abridged group

		void visitMain(NodeIndex n)
		{
			auto node = &nodes[n.val];
			if (node.main)
				return;
			if (node.kind.among(Node.Kind.ending, Node.Kind.recap))
				return;
			node.main = true;
			foreach (ref edge; node.edges)
				visitMain(edge.target);
		}
		visitMain(firstNode);

		changes = true;
		while (changes)
		{
			changes = false;
			foreach (i, ref node; nodes)
				if (!node.deleted && node.main && node.edges.length &&
					node.kind.among(Node.Kind.precondition, Node.Kind.preconditionResult) &&
					node.edges.all!(edge => !nodes[edge.target.val].main))
				{
					node.main = false;
					changes = true;
				}
		}
	}
}

string wrapNodeText(string html)
{
	assert(html.length, "Empty node!");
	if (draft)
		return html;

	static struct Word
	{
		string html;
		size_t width;
	}
	Word[] words;

	{
		Word word;
		bool inTag;

		foreach (i, dchar c; html)
		{
			if (inTag)
			{
				if (c == '>')
					inTag = false;
			}
			else
			if (c == '<')
				inTag = true;
			else
			if (c == ' ')
			{
				words ~= word;
				word = Word.init;
				continue;
			}
			else
				word.width++;
			word.html ~= c;
		}
		words ~= word;
	}

	string[] tryWrap(size_t width)
	{
		string[] lines;
		size_t currentWidth;
		foreach (ref word; words)
		{
			if (!lines || (currentWidth > 0 && currentWidth + word.width > width))
			{
				lines ~= null;
				currentWidth = 0;
			}
			if (lines[$-1].length)
				lines[$-1] ~= ' ';
			lines[$-1] ~= word.html;
			currentWidth += word.width;
		}
		return lines;
	}

	auto textWidth = words.map!(word => word.width).sum + words.length - 1;
	auto initWidth = cast(int)(sqrt(float(textWidth)) * 2).clamp(15, 30);

	auto width = initWidth;
	while (width && tryWrap(width - 1).length == tryWrap(width).length)
		width--;

	return tryWrap(width).join("<br/>").strip;
}

Graph getGraph()
{
	Graph g;
	alias Node = Graph.NodeIndex;
	alias NodeKind = Graph.Node.Kind;
	alias EdgeKind = Graph.Edge.Kind;
	alias NodeFormat = Graph.Node.Format;

	auto choicePoints = video.choicePointNavigatorMetadata.choicePointsMetadata.choicePoints;

	static NodeKind segmentNodeKind(string label)
	{
		// TODO: unlocalizable
		if (label == "Rewind!" || label.startsWith("Fast-forward") || label.startsWith("(Abridged)"))
			return NodeKind.recap;
		if (label.startsWith("<b>Game Over.</b>") || label.startsWith("<b>The End.</b>") ||
			label.startsWith("<b>Left:</b>") || label.startsWith("<b>Right:</b>") || label.startsWith("<b>TV:</b>") ||
			label.startsWith("Main credits") || label == "Remaining credits.")
			return NodeKind.ending;
		return NodeKind.segment;
	}

	// Pre-create segment and segment group nodes
	Node[string] segmentNodes, segmentGroupNodes;
	auto segments = segmentMap.segments.byPair.array.sort!((a, b) => a[1].startTimeMs < b[1].startTimeMs);
	foreach (id, segment; segments)
	{
		auto description = textData.segmentDescriptions.get(id, null).findSplit("#")[0].strip;
		auto kind = segmentNodeKind(description);
		// foreach (moment; video.momentsBySegment.get(id, null))
		// 	if (moment.type != "notification:playbackImpression" && !moment.config.disableImmediateSceneTransition)
		// 		kind = NodeKind.ending;
		auto tooltip = format("Segment ID: %s\nStart: %s\nEnd: %s", id, formatTimestamp(segment.startTimeMs), formatTimestamp(segment.endTimeMs));
		Node node;
		if (description)
			node = g.addNode(kind, description.wrapNodeText, NodeFormat.html, tooltip);
		else
		{
			stderr.writefln("Missing description for segment %s", id);
			node = g.addNode(kind, id, NodeFormat.text, tooltip);
		}
		segmentNodes[id] = node;
	}
	foreach (id, data; video.segmentGroups.byPair.array.sort)
		segmentGroupNodes[id] = g.addNode(NodeKind.segmentGroup, "sg_" ~ id);
	g.firstNode = segmentNodes[segmentMap.initialSegment];

	bool[string] findPreconditionVars(JSONValue json)
	{
		bool[string] seenVars;
		void scan(JSONValue v)
		{
			if (v.type == JSONType.array)
			{
				if (v[0].str == "persistentState")
					seenVars[v[1].str] = true;
				else
					foreach (c; v.array[1..$])
						scan(c);
			}
		}
		scan(json);
		return seenVars;
	}

	static JSONValue evalPrecondition(JSONValue json, scope JSONValue delegate(string) getPersistentState)
	{
		JSONValue eval(JSONValue v)
		{
			if (v.type == JSONType.false_ || v.type == JSONType.true_ || v.type == JSONType.string)
				return v;
			else
			if (v.type == JSONType.array)
				switch (v.array[0].str)
				{
					case "persistentState":
						assert(v.array.length == 2);
						return getPersistentState(v[1].str);
					case "not":
						assert(v.array.length == 2);
						return JSONValue(!eval(v[1]).boolean);
					case "and":
						return JSONValue(v.array[1..$].map!(c => eval(c).boolean).all);
					case "or":
						return JSONValue(v.array[1..$].map!(c => eval(c).boolean).any);
					case "eql":
						assert(v.array.length == 3);
						return JSONValue(eval(v[1]) == eval(v[2]));
					default:
						assert(false);
				}
			else
				assert(false);
		}
		return eval(json);
	}

	static ref T cached(T, Keys...)(scope T delegate() calculate, auto ref Keys keys)
	{
		static struct CacheKey { Keys keys; }
		static T[CacheKey] cache;
		return cache.require(CacheKey(keys), calculate());
	}

	Node[][] generateDecisionTree(Outcome)(string[] vars, size_t numOutcomes, scope Outcome delegate(JSONValue[] values) evalOutcome, Node[] parents)
	{
		JSONValue[][] varValues = vars.map!(var => allVariableValues[var].keys.sort.map!parseJSON.array).array;
		auto numCombinations = varValues.map!(values => values.length).fold!((a, b) => a * b)(size_t(1));

		auto results = cached({
			auto results = new Outcome[numCombinations];
			{
				auto currentValues = new JSONValue[vars.length];

				foreach (combination; 0 .. numCombinations)
				{
					auto rem = combination;
					// Incrementing combination index should mutate last variables,
					// so iterate in reverse order.
					foreach_reverse (varIndex, ref value; currentValues)
					{
						auto numValues = varValues[varIndex].length;
						auto valueIndex = rem % numValues;
						value = varValues[varIndex][valueIndex];
						rem /= numValues;
					}
					results[combination] = evalOutcome(currentValues);
				}
			}
			return results;
		}, vars, numOutcomes); // TODO: this cache key is probably unsound (hidden state behind evalOutcome not in cache key)..

		auto outcomes = new Node[][numOutcomes];

		void gen(uint varIndex, Node[] parents, immutable Outcome[] slice)
		{
			if (varIndex == vars.length)
			{
				assert(slice.length == 1);
				outcomes[slice[0]] ~= parents;
			}
			else
			{
				auto numValues = varValues[varIndex].length;
				assert(slice.length % numValues == 0);

				size_t[][Outcome[]] subslices;
				auto subsliceSize = slice.length / numValues;

				foreach (valueIndex; 0 .. numValues)
				{
					auto subslice = slice[valueIndex * subsliceSize .. (valueIndex + 1) * subsliceSize];
					subslices[subslice] ~= valueIndex;
				}

				if (subslices.length == 1)
				{
					// This variable is irrelevant (all possibilities
					// lead to the same outcome). No nodes generated.
					gen(varIndex + 1, parents, cast(immutable)subslices.byKey.front);
				}
				else
				{
					auto varName = vars[varIndex];
					Node varNode;
					if (varName in textData.vars && textData.vars[varName].readText)
						varNode = g.addNode(NodeKind.precondition, textData.vars[varName].readText.wrapNodeText, NodeFormat.html, varName);
					else
						varNode = g.addNode(NodeKind.precondition, varName ~ "?");

					foreach (parent; parents)
						g.addEdge(EdgeKind.normal, parent, varNode);

					// Sort by a subslice group's first value index
					foreach (pair; subslices.byPair.array.sort!((a, b) => a[1][0] < b[1][0]))
					{
						Node[] valueNodes;
						if (pair[1].all!(valueIndex => varValues[varIndex][valueIndex].type.among(JSONType.true_, JSONType.false_)))
						{
							foreach (valueIndex; pair[1])
							{
								auto value = varValues[varIndex][valueIndex];
								auto valueNode = g.addNode(NodeKind.dummy, null);
								g.addEdge(value.boolean ? EdgeKind.yes : EdgeKind.no, varNode, valueNode);
								valueNodes ~= valueNode;
							}
						}
						else
						{
							auto label = pair[1]
								.map!(valueIndex => varValues[varIndex][valueIndex].toString)
								.map!(varValue => textData.vars.get(varName, TextData.Var.init).readValueText.get(varValue, varValue))
								.join(" / ");
							auto valueNode = g.addNode(NodeKind.preconditionResult, label, NodeFormat.html);
							g.addEdge(EdgeKind.normal, varNode, valueNode);
							valueNodes = [valueNode];
						}
						gen(varIndex + 1, valueNodes, cast(immutable)pair[0]);
					}
				}
			}
		}
		gen(0, parents, cast(immutable)results);

		return outcomes;
	}

	// Returns "false" and "true" outcome branches.
	Node[][2] dumpPrecondition(JSONFragment precondition, Node[] parents)
	{
		auto json = parseJSON(precondition.json);
		auto seenVars = findPreconditionVars(json);
		auto vars = varReadOrder.filter!(var => var in seenVars).array;

		bool evalOutcome(JSONValue[] values)
		{
			JSONValue getVar(string varName)
			{
				auto varIndex = vars.countUntil(varName);
				assert(varIndex >= 0);
				return values[varIndex];
			}

			return evalPrecondition(json, &getVar).boolean;
		}

		auto outcomes = generateDecisionTree(vars, 2, &evalOutcome, parents);
		assert(outcomes.length == 2);
		return outcomes[0..2];
	}

	Node[] dumpImpressionData(ref ImpressionData impressionData, Node[] parents)
	{
		if (impressionData.data.persistent == video.stateHistory)
		{
			auto impressionNode = g.addNode(NodeKind.impression, "[reset all variables]");
			foreach (parent; parents)
				g.addEdge(EdgeKind.segment, parent, impressionNode);
			return [impressionNode];
		}

		foreach (k; varWriteOrder)
		{
			auto pv = k in impressionData.data.persistent;
			if (!pv)
				continue;
			auto v = *pv;

			Node impressionNode;
			auto value = v.json.strip;
			auto tooltip = format("%s := %s", k, v.json.strip);
			if (k in textData.vars && value in textData.vars[k].writeValueText)
				impressionNode = g.addNode(NodeKind.impression, textData.vars[k].writeValueText[value].wrapNodeText, NodeFormat.html, tooltip);
			else
				impressionNode = g.addNode(NodeKind.impression, tooltip);

			foreach (parent; parents)
				g.addEdge(EdgeKind.segment, parent, impressionNode);
			parents = [impressionNode];
		}
		return parents;
	}

	// Many preconditions for impressions are redundant (serve as an
	// optimization in the Netflix implementation), in the sense that
	// the outcome is the same whether the precondition is evaluated
	// or assumed to be true.
	bool isPreconditionRedundant(JSONFragment precondition, ref ImpressionData impressionData)
	{
		auto json = parseJSON(precondition.json);
		auto seenVars = findPreconditionVars(json);
		foreach (name, value; impressionData.data.persistent)
			seenVars[name] = true;

		auto vars = varReadOrder.filter!(var => var in seenVars).array;

		void applyImpression(JSONValue[] values)
		{
			foreach (varName, value; impressionData.data.persistent)
			{
				auto varIndex = vars.countUntil(varName);
				values[varIndex] = parseJSON(value.json);
			}
		}

		JSONValue[][] varValues = vars.map!(var => allVariableValues[var].keys.sort.map!parseJSON.array).array;
		auto input = new JSONValue[vars.length];
		auto outputWith = new JSONValue[vars.length];
		auto outputWithout = new JSONValue[vars.length];

		JSONValue getVar(string varName)
		{
			auto varIndex = vars.countUntil(varName);
			assert(varIndex >= 0);
			return input[varIndex];
		}

		auto numCombinations = varValues.map!(values => values.length).fold!((a, b) => a * b)(size_t(1));
		foreach (combination; 0 .. numCombinations)
		{
			auto rem = combination;
			foreach_reverse (varIndex, ref inputValue; input)
			{
				auto numValues = varValues[varIndex].length;
				auto valueIndex = rem % numValues;
				inputValue = varValues[varIndex][valueIndex];
				rem /= numValues;
			}

			outputWithout[] = input[];
			applyImpression(outputWithout);

			outputWith[] = input[];
			if (evalPrecondition(json, &getVar).boolean)
				applyImpression(outputWith);

			if (outputWith != outputWithout)
				return false;
		}
		return true;
	}

	static void convertImage(string fn, string target)
	{
		import std.process : spawnProcess, wait;
		enforce(spawnProcess(["convert", fn,
					"-bordercolor", "transparent",
					"-border", "10",
					"png:" ~ target]).wait() == 0, "convert failed");
	}

	static string getImage(ref Image image)
	{
		static CachedCurlNetwork net;
		if (!net)
			net = new CachedCurlNetwork();
		auto url = image.styles["backgroundImage"][4..$-1];
		auto path = net.downloadFile(url);
		auto target = path ~ ".png";
		.cached!convertImage(path, target);
		return target;
	}

	// Segments and moments
	foreach (id, segment; segmentMap.segments)
	{
		auto parents = [segmentNodes[id]];

		string[][] additionalNodes;
		if (auto pdesc = id in textData.segmentDescriptions)
			additionalNodes = (*pdesc).split("##").map!strip.map!(part => part.split("#").map!strip.array).array;

		if (additionalNodes.length)
			foreach (i, nodeText; additionalNodes[0][1..$])
			{
				auto node = g.addNode(segmentNodeKind(nodeText), nodeText.wrapNodeText, NodeFormat.html,
					"Continuation of segment %s (node %d)".format(id, i));
				foreach (parent; parents)
					g.addEdge(EdgeKind.segment, parent, node);
				parents = [node];
			}

		auto moments = video.momentsBySegment.get(id, null);
		foreach (momentIndex, moment; moments)
		{
			assert(parents.length, "Unreachable moment");

			auto momentParents = parents;
			parents = null;

			if (moment.precondition.json)
			{
				// TODO: causes an assertion failure
				auto isRedundant = /*optimize &&*/ !moment.choices && isPreconditionRedundant(moment.precondition, moment.impressionData);
				if (!isRedundant)
				{
					auto preconditionOutcomes = dumpPrecondition(moment.precondition, momentParents);
					parents = preconditionOutcomes[0];
					momentParents = preconditionOutcomes[1];
				}
			}

			momentParents = dumpImpressionData(moment.impressionData, momentParents);

			if (moment.choices)
			{
				string choicesText;
				if (auto desc = textData.momentDescriptions.get(id, null).get(momentIndex, null))
					choicesText = desc;
				if (!choicesText && moment.id in choicePoints && choicePoints[moment.id].description)
				{
					auto timeDelta = moment.startMs - choicePoints[moment.id].startTimeMs;
					if (timeDelta > 0 && timeDelta < 7000)
						choicesText = choicePoints[moment.id].description;
				}
				if (!choicesText)
					choicesText = "Choice";
				auto choicesNode = g.addNode(NodeKind.choices, choicesText);
				foreach (parent; momentParents)
					g.addEdge(EdgeKind.segment, parent, choicesNode);
				momentParents = [];

				foreach (choiceIndex, choice; moment.choices)
				{
					auto target = choice.segmentId ? segmentNodes[choice.segmentId] :
						choice.sg ? segmentGroupNodes[choice.sg] :
						Node.init;

					Node choiceNode;
					if (moment.inputConfig.json)
						choiceNode = g.addNode(NodeKind.choice, choice.code ? choice.code : "(anything else)");
					else
					if (choice.image.styles)
						choiceNode = g.addNode(NodeKind.choice, getImage(choice.image), NodeFormat.image);
					else
						choiceNode = g.addNode(NodeKind.choice, choice.text);

					g.addEdge(choiceIndex == moment.defaultChoiceIndex ? EdgeKind.choiceDefault : EdgeKind.normal, choicesNode, choiceNode);

					choiceNode = dumpImpressionData(choice.impressionData, [choiceNode])[0];

					if (target)
					{
						bool forward = true;
						// if (!moment.config.disableImmediateSceneTransition && choice.optionType =! "credits")
						// 	forward = false;
						auto segmentDesc = textData.segmentDescriptions.get(id, null);
						if (segmentDesc.startsWith("<b>Game Over.</b>") || segmentDesc.startsWith("<b>The End.</b>"))
							forward = false;
						if (choice.optionType == "credits")
							forward = true;
						if (choice.segmentId && segmentMap.segments[choice.segmentId].credits)
							forward = true;
						g.addEdge(EdgeKind.normal, choiceNode, target, null, !forward);
					}
					else
						momentParents ~= choiceNode;
				}
			}
			parents ~= momentParents;

			if (additionalNodes.length > 1 + momentIndex)
				foreach (i, nodeText; additionalNodes[1 + momentIndex])
				{
					assert(parents.length, "Moment %d of %s has no parents to attach additional notes to".format(momentIndex, id));
					auto node = g.addNode(segmentNodeKind(nodeText), nodeText.wrapNodeText, NodeFormat.html,
						"Continuation of segment %s after moment %d (node %d)".format(id, momentIndex, i));
					foreach (parent; parents)
						g.addEdge(EdgeKind.segment, parent, node);
					parents = [node];
				}
		}

		assert(additionalNodes.length <= 1 + moments.length, "Excess additional notes for moments for " ~ id);

		if (parents.length)
		{
			// TODO: join parents
			foreach (parent; parents)
			{
				if (id in video.segmentGroups)
					g.addEdge(EdgeKind.segmentGroupDefault, parent, segmentGroupNodes[id]);
				else
				if (segment.defaultNext)
					g.addEdge(EdgeKind.nextDefault, parent, segmentNodes[segment.defaultNext]);
			}
		}
	}

	// Segment groups
	foreach (id, data; video.segmentGroups)
	{
		// Parse into something more manageable.
		struct SegmentGroupElement
		{
			JSONValue precondition;
			Node target;
		}
		auto elements = new SegmentGroupElement[data.length];

		foreach (elementIndex, ref element; elements)
		{
			auto datum = data[elementIndex];
			auto json = parseJSON(datum.json);
			if (json.type == JSONType.string)
			{
				if (json.str in video.preconditions)
					element.precondition = parseJSON(video.preconditions[json.str].json);
				element.target = segmentNodes[json.str];
			}
			else
			if (json.type == JSONType.object)
			{
				if (json.object.keys.sort.equal(["precondition", "segment"]))
				{
					element.precondition = parseJSON(video.preconditions[json["precondition"].str].json);
					element.target = segmentNodes[json["segment"].str];
				}
				else
				if (json.object.byKey.equal(["segmentGroup"]))
					element.target = segmentGroupNodes[json["segmentGroup"].str];
				else
					assert(false, datum.json);
			}
			else
				assert(false, datum.json);
			elements[elementIndex] = element;
		}

		bool[string] seenVars;
		foreach (ref element; elements)
			if (element.precondition !is JSONValue.init)
				foreach (name; findPreconditionVars(element.precondition).byKey)
					seenVars[name] = true;
		auto vars = varReadOrder.filter!(var => var in seenVars).array;

		size_t evalOutcome(JSONValue[] values)
		{
			foreach (elementIndex, ref element; elements)
			{
				if (element.precondition !is JSONValue.init)
				{
					JSONValue getVar(string varName)
					{
						auto varIndex = vars.countUntil(varName);
						assert(varIndex >= 0);
						return values[varIndex];
					}

					if (!evalPrecondition(element.precondition, &getVar).boolean)
						continue;
				}
				return elementIndex;
			}
			//assert(false, "No preconditions matched");
			return elements.length;
		}

		auto outcomes = generateDecisionTree(vars, elements.length + 1, &evalOutcome, [segmentGroupNodes[id]]);

		foreach (elementIndex, ref element; elements)
		{
			auto outcomeNodes = outcomes[elementIndex];
			//assert(outcomeNodes.length, "Unreachable outcome");
			if (outcomeNodes.length)
				foreach (outcomeNode; outcomeNodes)
					g.addEdge(EdgeKind.normal, outcomeNode, element.target);
			else
			{
				auto node = g.addNode(NodeKind.error, "Unreachable outcome");
				g.addEdge(EdgeKind.unreachable, segmentGroupNodes[id], node);
				g.addEdge(EdgeKind.unreachable, node, element.target);
			}
		}
		if (outcomes[elements.length].length)
		{
			auto node = g.addNode(NodeKind.error, "Impossible outcome?");
			foreach (outcomeNode; outcomes[elements.length])
				g.addEdge(EdgeKind.normal, outcomeNode, node);
		}
	}

	foreach (ref node; g.nodes)
		if (node.kind == NodeKind.segmentGroup)
			node.kind = node.edges.length == 1 ? NodeKind.dummy : NodeKind.point;

	g.finalize();

	return g;
}

void genGraph()
{
	getGraph().put(File("graph.dot", "w"));
}

void optimizeVarList(string[] vars, string fn)
{
	auto bestOrder = vars.dup;
	auto bestScore = ulong.max;

	bool tryOrder()
	{
		auto g = getGraph();
		ulong score;
		foreach (ref node; g.nodes)
			if (!node.deleted)
			{
				score++;
				score += node.edges.length;
			}
		if (bestScore > score)
		{
			stderr.writefln("New best order! %d -> %d", bestScore, score);
			bestScore = score;
			bestOrder[] = vars[];
			File(fn, "w").writef("%-(%s\n%)", bestOrder);
			return true;
		}
		return false;
	}

	tryOrder(); // Set initial score

searchLoop:
	while (true)
	{
		foreach (i; 0 .. vars.length)
			foreach (j; i + 1 .. vars.length)
			{
				stderr.writefln("Trying to swap %d <-> %d", i, j);
				vars[] = bestOrder[];
				swap(vars[i], vars[j]);
				if (tryOrder())
					continue searchLoop;
			}

		foreach (i; 0 .. vars.length)
			foreach (j; 0 .. vars.length)
			{
				if (i == j)
					continue;
				stderr.writefln("Trying to shift %d -> %d", i, j);
				vars[] = bestOrder[];
				if (j > i)
					foreach (n; 0 .. j - i)
						swap(vars[i + n], vars[i + n + 1]);
				else
					foreach (n; 0 .. i - j)
						swap(vars[i - n], vars[i - n - 1]);
				if (tryOrder())
					continue searchLoop;
			}

		break;
	}
}

void optimizeVars()
{
	draft = true;
	optimizeVarList(varReadOrder , bestVariableReadOrderFN );
	optimizeVarList(varWriteOrder, bestVariableWriteOrderFN);
}

void genDataTemplates()
{
	auto segments = segmentMap.segments.byPair.array.sort!((a, b) => a[1].startTimeMs < b[1].startTimeMs);

	auto f = File("data.tpl.org", "w");
	f.writeln("* Segments");
	f.writeln();
	f.writefln("|-");
	f.writefln("| ID | Start | Duration | Choices | Description |");
	f.writefln("|-");
	foreach (ref pair; segments)
	{
		auto id = pair[0];
		auto segment = pair[1];
		f.writefln("| %s | %s | %s | %-(%s/%) | |",
			id,
			segment.startTimeMs.formatTimestamp,
			segment.endTimeMs ? (segment.endTimeMs - segment.startTimeMs).formatTimestamp : "-",
			video.momentsBySegment.get(id, null)
			.filter!(moment => moment.choices.length)
			.array
			.sort!((a, b) => a.startMs > b.startMs)
			.takeOne
			.map!(moment => moment.choices)
			.joiner
			.map!(choice => choice.text),
		);
	}
	f.writefln("|-");
	f.writeln();

	f.writeln("* Variables");
	f.writeln();
	f.writefln("|-");
	f.writefln("| Name | Def. | Description | Values |");
	f.writefln("|-");
	foreach (name; video.stateHistory.keys.sort)
	{
		string[] writes, reads;
		bool[string] values;

		bool scanPrecondition(JSONFragment fragment)
		{
			bool scan(JSONValue j)
			{
				if (j.type == JSONType.array)
					return j.array.any!scan;
				if (j.type == JSONType.string)
					return j.str == name;
				return false;
			}
			return scan(parseJSON(fragment.json));
		}

		bool scanImpressionData(ref ImpressionData impressionData)
		{
			if (auto pval = name in impressionData.data.persistent)
			{
				values[pval.json] = true;
				return true;
			}
			return false;
		}

		foreach (ref pair; segments)
		{
			auto id = pair[0];
			foreach (i, moment; video.momentsBySegment.get(id, null))
			{
				if (scanImpressionData(moment.impressionData))
					writes ~= format("%s/%d", id, i);
				if (scanPrecondition(moment.precondition))
					reads ~= format("%s/%d", id, i);
				foreach (j, choice; moment.choices)
					if (scanImpressionData(choice.impressionData))
						writes ~= format("%s/%d/%d", id, i, j);
			}
		}

		foreach (name, value; video.preconditions)
			if (scanPrecondition(value))
				reads ~= "pc:" ~ name;

		f.writefln("| %s | %s | | %-(%s | %) |",
			name,
			video.stateHistory[name].json,
			values.keys.sort,
		);
	}
	f.writefln("|-");

	auto choicePoints = video.choicePointNavigatorMetadata.choicePointsMetadata.choicePoints;

	f.writeln("* Moments");
	f.writeln();
	f.writefln("|-");
	f.writefln("| Segment | # | ID | Choices | Choice point description | Description |");
	f.writefln("|-");
	foreach (pairs; video.momentsBySegment.byPair.array.sort!((a, b) => a[1][0].startMs < b[1][0].startMs))
	{
		auto segmentId = pairs[0];
		foreach (momentIndex, ref moment; pairs[1])
			if (moment.choices)
			{
				string choicesText;
				if (moment.id in choicePoints && choicePoints[moment.id].description)
				{
					auto timeDelta = moment.startMs - choicePoints[moment.id].startTimeMs;
					if (timeDelta > 0 && timeDelta < 7000)
						choicesText = choicePoints[moment.id].description;
				}

				f.writefln("| %s | %d | %s | %-(%s/%) | %s | |",
					segmentId,
					momentIndex,
					moment.id,
					moment.choices.map!(choice => choice.text),
					choicesText,
				);
			}
	}
	f.writefln("|-");
}

void genHTML()
{
	// foreach (k, v; bandersnatch.videos["80988062"].interactiveVideoMoments.value.momentsBySegment)
	// 	//assert(v.length == 1, k);
	// 	foreach (m; v)
	// 		switch (m.type)
	// 		{
	// 			case "scene:cs_bs":
	// 			case "notification:playbackImpression":
	// 				break;
	// 			default:
	// 				writeln(m.type);
	// 		}

	// auto moments = video.momentsBySegment.values.join;
	// moments.sort!((a, b) => a.startMs < b.startMs);

	auto choicePoints = video.choicePointNavigatorMetadata.choicePointsMetadata.choicePoints;

	auto f = File("out.html", "w");
	f.writeln("<html><body>");

	// foreach (id, moments; video.momentsBySegment)
	// 	assert(moments.filter!(m => m.type != "notification:playbackImpression"))

	void dumpImpressionData(ref ImpressionData id)
	{
		if (id is ImpressionData.init)
			return;
		assert(id.type == "userState", id.type);
		foreach (k, v; id.data.persistent)
			f.writefln("<li>Sets persistent flag <b>%s</b> to <b>%s</b></li>", k.encodeEntities, v.json.encodeEntities);
	}

	f.writefln(`<a href="#%s">Start</a>`, segmentMap.initialSegment);

	f.writeln(`<h1>Segments</h1>`);

	foreach (ref pair; segmentMap.segments.byPair.array.sort!((a, b) => a[1].startTimeMs < b[1].startTimeMs))
	{
		auto id = pair[0];
		auto segment = pair[1];
		f.writefln(`<h2 id="%s"><a href="#%s">%s</a></h2>`, id, id, id);

		f.writeln("<ul>");

		f.writefln("<li>Span: %s - %s</li>", segment.startTimeMs.formatTimestamp, segment.endTimeMs.formatTimestamp);

		f.writeln("<li>Interaction zones:<ul>");
		foreach (zone; segment.ui.interactionZones)
			f.writefln("<li>%s - %s</li>", zone[0].formatTimestamp, zone[1].formatTimestamp);
		f.writeln("</ul></li>");

		f.writeln("<li>Next:<ul>");
		foreach (id, next; segment.next)
			f.writefln(`<li><a %2$shref="#%1$s">%1$s</a> (weight: %3$d)</li>`,
				id,
				id == segment.defaultNext ? `style="font-weight: bold" ` : ``,
				next.weight,
			);
		f.writeln("</ul></li>");

		if (segment.storyEnd)
			f.writeln("<li><b>STORY END</b></li>");
		if (segment.credits)
			f.writeln("<li><b>Credits!</b></li>");

		f.writeln("<li>Moments:<ul>");
		foreach (moment; video.momentsBySegment.get(id, null))
		{
			// if (moment.type == "notification:playbackImpression")
			// 	continue;
			bool isNotification = moment.type.startsWith("notification:");

			f.writefln("<li>%s<br>", moment.id);
			if (moment.id in choicePoints && choicePoints[moment.id].image.styles)
				f.writefln(`<img src="%s">`, choicePoints[moment.id].image.styles["backgroundImage"][4..$-1]);
			f.writeln(`<ul>`);

			if (moment.type != "scene:cs_bs")
				f.writefln("<li>Type: %s</li>", moment.type.encodeEntities);
			if (moment.startMs != segment.startTimeMs || moment.endMs != segment.endTimeMs)
				f.writefln("<li>Span: %s - %s</li>", moment.startMs.formatTimestamp, moment.endMs.formatTimestamp);
			if (moment.precondition.json)
				f.writefln("<li>Precondition: %s</li>", moment.precondition.json.encodeEntities);
			// string[string] trackingInfo;
			if (isNotification
				? moment.activationWindow != [0, 0]
				: moment.activationWindow != [moment.startMs, moment.endMs - 4000])
				f.writefln("<li>Activation window: %s - %s</li>", moment.activationWindow[0].formatTimestamp, moment.activationWindow[1].formatTimestamp);
			if (moment.segmentId && moment.segmentId != moment.id)
				assert(false);
			if (isNotification
				? moment.layoutType !is null
				: moment.layoutType != "l2")
				f.writefln("<li>Layout type: %s</li>", moment.layoutType.encodeEntities);
			if (isNotification
				? moment.uiDisplayMS || moment.uiHideMS
				: moment.uiDisplayMS != moment.startMs + 3640 || moment.uiHideMS != moment.activationWindow[1])
				f.writefln("<li>UI span: %s - %s</li>", moment.uiDisplayMS.formatTimestamp, moment.uiHideMS.formatTimestamp);
			if (moment.choiceActivationThresholdMS != moment.activationWindow[0])
				f.writefln("<li>Choice activation threshold: %s</li>", moment.choiceActivationThresholdMS);
			if (moment.uiInteractionStartMS != moment.uiDisplayMS)
				f.writefln("<li>UI interaction start: %s</li>", moment.uiInteractionStartMS);

			if (moment.config.intervalBasedVideoTimer != (isNotification ? false : true))
				f.writefln("<li>Interval-based video timer: %s</li>", moment.config.intervalBasedVideoTimer);
			if (moment.config.disableImmediateSceneTransition != (isNotification || id.startsWith("SS") ? false : true))
				f.writefln("<li>Disable immediate scene transition: %s</li>", moment.config.disableImmediateSceneTransition);
			if (moment.config.disablePrematureUserInteraction != (isNotification ? false : true))
				f.writefln("<li>Disable premature user interaction: %s</li>", moment.config.disablePrematureUserInteraction);
			if (moment.config.hideChoiceLabelWhenChoiceHasImage != (isNotification ? false : true))
				f.writefln("<li>Hide choice label when choice has image: %s</li>", moment.config.hideChoiceLabelWhenChoiceHasImage);
			if (moment.config.randomInitialDefault != (isNotification || segment.storyEnd ? false : true))
				f.writefln("<li>Random initial default: %s</li>", moment.config.randomInitialDefault);
			if (moment.config.defaultSelectType)
				f.writefln("<li>Default select type: %s</li>", moment.config.defaultSelectType.encodeEntities);
			if (moment.config.hasMultipleChoiceInput)
				f.writefln("<li>Has multiple choice input</li>");
			if (moment.inputConfig.json)
				f.writefln("<li>Input config: %s</li>", moment.inputConfig.json.encodeEntities);
			if (moment.action)
				f.writefln("<li>Action: %s</li>", moment.action.text.encodeEntities);
			dumpImpressionData(moment.impressionData);

			if (moment.id in choicePoints && choicePoints[moment.id].description)
				f.writefln("<li>Description: %s</li>", choicePoints[moment.id].description);
			if (moment.choices)
			{
				f.writeln("<li>Choices:<ul>");
				assert(moment.choices.length <= 2, id);
				foreach (i, choice; moment.choices)
				{
					assert(choice.id);
					assert((choice.sg is null) || (choice.segmentId is null), [choice.id, choice.sg, choice.segmentId].text);
					f.writef("<li>%s", choice.id);
					if (choice.text)
						f.writef(" - %s", choice.text.encodeEntities);
					if (i == moment.defaultChoiceIndex && moment.choices.length > 1)
						f.write(" [default]");
					f.writeln("<br>");
					if (choice.image.styles)
						f.writefln(`<img src="%s"><br>`, choice.image.styles["backgroundImage"][4..$-1]);
					f.writeln("<ul>");
					if (choice.optionType)
						f.writefln(`<li>Type: %s</li>`, choice.optionType.encodeEntities);
					if (choice.code)
						f.writefln(`<li>Code: %s</li>`, choice.code.encodeEntities);
					if (choice.sg)
						f.writefln(`<li>Segment group: <a href="#sg_%1$s">%1$s</a></li>`, choice.sg.encodeEntities);
					if (choice.segmentId)
						f.writefln(`<li>Segment: <a href="#%1$s">%1$s</a></li>`, choice.segmentId.encodeEntities);
					if (choice.startTimeMs != (choice.segmentId ? segmentMap.segments[choice.segmentId].startTimeMs : 0))
						f.writefln(`<li>Start time: %s</li>`, choice.startTimeMs.formatTimestamp);
					dumpImpressionData(choice.impressionData);
					// JSONFragment trackingInfo;
					f.writeln("</ul>");
				}
				f.writeln("</ul></li>");
			}
			f.writeln(`</ul></li>`);
		}
		f.writeln("</ul></li>"); // Moments

		if (id in video.segmentGroups)
			f.writefln(`<li>Has corresponding segment group: <a href="#ss_%1$s">%1$s</a></li>`, id);
		if (id in video.preconditions)
			f.writefln("<li>Has corresponding precondition: %s</li>",
				video.preconditions[id].json.encodeEntities);

		f.writeln("</ul>");
	}

	f.writeln(`<h1>Segment groups</h1>`);
	foreach (id, data; video.segmentGroups)
	{
		f.writefln(`<h2 id="sg_%1$s"><a href="#sg_%1$s">%1$s</a></h2><ul>`, id.encodeEntities);
		foreach (datum; data)
		{
			auto json = parseJSON(datum.json);
			if (json.type == JSONType.string)
				f.writefln(`<li><a href="#%1$s">%1$s</a></li>`, json.str.encodeEntities);
			else
			if (json.type == JSONType.object)
			{
				if (json.object.keys.sort.equal(["precondition", "segment"]))
					f.writefln(`<li><a href="#%1$s">%1$s</a> (precondition %2$s: %3$s)</li>`,
						json["segment"].str.encodeEntities,
						json["precondition"].str.encodeEntities,
						video.preconditions[json["precondition"].str].json.encodeEntities,
					);
				else
				if (json.object.byKey.equal(["segmentGroup"]))
					f.writefln(`<li>Segment group <a href="#sg_%1$s">%1$s</a></li>`,
						json["segmentGroup"].str.encodeEntities,
					);
				else
					assert(false, datum.json);
			}
			else
				assert(false, datum.json);
		}
		f.writeln("</ul>");
	}
}

void checkContiguity()
{
	auto seen = new string[6 * 60 * 60 * 1000];
	foreach (name, segment; segmentMap.segments)
	{
		auto seg = seen[segment.startTimeMs .. segment.endTimeMs ? segment.endTimeMs : $];
		foreach (s; seg)
			if (s)
			{
				writefln("Overlapping segment: %s / %s", name, s);
				break;
			}
		seg[] = name;
	}
	foreach (i, s; seen)
		if (!s)
			writeln("Uncovered fragment at ", i);
}

string formatTimestamp(long ms)
{
	return format("%d:%02d:%02d.%03d",
		ms / 1000 / 60 / 60,
		ms / 1000 / 60 % 60,
		ms / 1000 % 60,
		ms % 1000,
	);
}
